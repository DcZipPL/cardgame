using System.Collections;
using System.Collections.Generic;
using Unity.VectorGraphics;
using UnityEngine;

[CreateAssetMenu(fileName = "Card", menuName = "ScriptableObjects/Card", order = 0)]
public class Card : ScriptableObject
{
	public Sprite backSprite;
	public Sprite frontSprite;

	public Sprite image;
	public string cardName;
	[Multiline]
	public string cardDesc;

	public float health;
	public int mana;
	public int damage;
}