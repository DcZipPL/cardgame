using System.Collections;
using System.Collections.Generic;
using Unity.VectorGraphics;
using UnityEngine;

public class GameLogic : MonoBehaviour
{

    public bool alreadySelected = false;

    public List<GameObject> uiCards;

    public GameObject gamePanel;
    public List<Card> cards;

    GameObject igamePanel;

    // Start is called before the first frame update
    void Start()
    {
        if (uiCards.Count == 0)
        {
            Debug.LogError("Missing cards UI!");
        }

        if (cards.Count == 0)
        {
            Debug.LogError("No cards!");
        }


        for (int i = 0; i < uiCards.Count; i++)
        {
            print(i);
            igamePanel = Instantiate(gamePanel, uiCards[i].transform);

            igamePanel.transform.Find("Back SVG Image").GetComponent<SVGImage>().sprite = cards[i].backSprite;

            Transform cachedFrontPanel = igamePanel.transform.Find("Front SVG Image");
            cachedFrontPanel.gameObject.SetActive(false);
            cachedFrontPanel.GetComponent<SVGImage>().sprite = cards[i].frontSprite;
            cachedFrontPanel.Find("Card Name").GetComponent<TMPro.TMP_Text>().text =   cards[i].cardName;
            cachedFrontPanel.Find("Card Desc").GetComponent<TMPro.TMP_Text>().text =   cards[i].cardDesc;
            cachedFrontPanel.Find("Card Health").GetComponent<TMPro.TMP_Text>().text = cards[i].health + "";
            cachedFrontPanel.Find("Card Mana").GetComponent<TMPro.TMP_Text>().text =   cards[i].mana + "";
            cachedFrontPanel.Find("Card Damage").GetComponent<TMPro.TMP_Text>().text = cards[i].damage + "";
        }

    }

    // Update is called once per frame
    void Update()
    {
    }

    public static GameLogic getInstance()
    {
        return GameObject.Find("Game Logic").GetComponent<GameLogic>();
    }
}
