using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardEntity : MonoBehaviour, IPointerDownHandler
{
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        GameLogic instance = GameLogic.getInstance();
        if (!instance.alreadySelected)
        {
            gameObject.GetComponent<Animator>().SetBool("clicked",true);
            instance.alreadySelected = true;
        }
    }
}
